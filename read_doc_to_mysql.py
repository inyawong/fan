import binascii
import os
import linkSql as sql
# import hashlib

def read_doc_to_bytes(doc_file:str):
    if doc_file.endswith(".doc") == False:
        if doc_file.endswith(".docx") == False:
            return None
    
    tg_file = open(doc_file, "rb+")
    tg_bytes = tg_file.read()
    tg_file.close()
    return tg_bytes


def hex_str_to_bytes(hex_str:str):
    if hex_str == None:
        return None
    return bytes.fromhex(hex_str)

def bytes_to_str_hex(bytes_val:bytes):
    if bytes_val == None:
        return None
    return binascii.hexlify(bytes_val).decode('utf-8')


def is_file_exists(path_m):
    return os.path.exists(path_m)

def delete_file(path_m):
    if is_file_exists(path_m):
        os.remove(path_m)

def write_doc_bytes_to_file(output_doc_path, byte_doc:bytes):
    delete_file(output_doc_path)
    tg_file = open(output_doc_path, "wb")
    tg_file.write(byte_doc)
    tg_file.close()
    return 

def get_file_list(dir_path):
    total_file_list = []
    for i in os.listdir(dir_path):
        path_file = os.path.join(dir_path, i)
        if os.path.isfile(path_file):
            total_file_list = total_file_list + [path_file]
        else:
            sub_path = get_file_list(path_file)
            total_file_list = total_file_list + sub_path

    return total_file_list


def read_dir_doc_to_map(tg_dir):
    
    file_list = get_file_list(tg_dir)
    file_list = list(set(file_list))
    
    doc_and_str_hex = {}
    
    for file_t in file_list:
        if file_t.endswith(".doc") == False:
            if file_t.endswith(".docx") == False:
                continue
            
        doc_bytes = read_doc_to_bytes(file_t)
        if doc_bytes == None:
            print("error: read file -> ", file_t)
            continue
        
        # 1、table col type is string， so we cover bytes to string
        # 2、some special byte(like ' = ") will affect mysql query language.other like base64 encode.
        str_hex = bytes_to_str_hex(doc_bytes)
        
        if file_t not in doc_and_str_hex.keys():
            doc_and_str_hex[file_t] = str_hex
    
    return doc_and_str_hex


# sql_result -> ...
# doc_index_number -> which is index(col), some data need to decode  
# get_true_doc_bin -> decode data
def get_true_doc_bin(sql_result:list, doc_index_number:int):
    
    if sql_result.__len__() <=  doc_index_number:
        return None
    
    doc_str_hex_need_decode = sql_result[doc_index_number]
    
    doc_bytes = hex_str_to_bytes(doc_str_hex_need_decode)
    # print(sql_result[0:doc_index_number])
    # exit()
    
    return_list = [] + sql_result[0:doc_index_number] + [doc_bytes] + sql_result[doc_index_number+1:]
    
    return return_list


def get_all_true_doc_bin(sql_result_all:list, doc_index_number:int):
    
    return_list = []
    for sql_result_t in sql_result_all:
        return_list = return_list + [get_true_doc_bin(sql_result_t, doc_index_number)]
        
    return return_list

# def get_file_md5(filename):
#     if not os.path.isfile(filename):
#         return None
#     myhash = hashlib.md5()
#     f = open(filename, 'rb')
#     while True:
#         b = f.read()
#         if not b:
#             break
#         myhash.update(b)
#     f.close()
#     return myhash.hexdigest()



def test_insert_a_doc(id_t, name_str, file_path):

    cursor = sql.db.cursor()

    key_t = bytes(file_path, "utf-8")
    key_t = bytes_to_str_hex(key_t)

    str_hex = bytes_to_str_hex(read_doc_to_bytes(file_path))  # type: ignore
    # str_hex = 

    sql_insert = "insert into save_doc (id,m_name, doc_name, doc_hex) values('%s','%s','%s','%s')" \
    %(id_t,name_str, key_t, str_hex)
    cursor.execute(sql_insert)
    sql.db.commit()   # 一定要commit

    cursor.close()
    print('插入数据成功')
    return 1


def test_query_a_doc(col_name, value_t):
    cursor = sql.db.cursor()

    # sql_sel = "select * from mover where id='%s'"%id
    sql_sel = "select id,m_name, doc_name, doc_hex from save_doc where %s=%s" % (col_name, value_t)
    result = cursor.execute(sql_sel)
    # print(cursor.fetchall())
    if result:
        mov = cursor.fetchall()
        cursor.close()
        return mov    
    else:
        print("未找到您的信息")

    cursor.close()
    return None


def test_query_doc_to_local(col_name:str, col_value:str, output_to_file:str):
    mov = test_query_a_doc(col_name, col_value)

    # mov -> ((id,m_name, doc_name, doc_hex))
    if mov == None:
        print("没有查询到内容")

    for one_case in list(mov):  # type: ignore
        # test_query_a_doc return -> Encoded. we need Decode.
        # Encode \x21\x22  -> str: 2122
        # \x21\x22 -> bytes -> memory 
        one_case = get_true_doc_bin(list(one_case), 3)
        one_case = get_true_doc_bin(list(one_case), 2)
    
        file_path: str = list(one_case)[2]  # type: ignore
        file_data = list(one_case)[3]  # type: ignore

        # print(file_data)
        # print(file_path)

        # in data covering, the last result data type is bytes,,but file_path need a string. 
        file_path = str(file_path, "utf-8")  # type: ignore

        
        # file_path = file_path.replace("\\",)
        write_doc_bytes_to_file(output_to_file, file_data)




# test_insert_a_doc(2, "goudaner", "C:\\Users\\青山皊\\Desktop\\for_myff\\b_test_.docx")

# exit()

# test_insert_a_doc("E:\\fan\\fan\\template")

# test_query_doc_to_local("id", "2", "C:\\Users\\青山皊\\Desktop\\for_myff\\out_put.docx")
# print(mov)
exit()

    
    