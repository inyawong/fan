# -*- coding:utf-8 -*-

import tkinter as tk
from tkinter import *  # type: ignore
import tkinter # type: ignore
from PIL import Image, ImageTk
 
class radiobutton:
    def __init__(self):
        window = Tk()
        window.title("文档生成页面") 
        window.geometry("800x500") 

        #背景图
        img=Image.open('word and pdf.png')  # type: ignore
        photo=ImageTk.PhotoImage(img)
        Label(window,image=photo).grid(row=0,column=0)

        # 鼠标点击到单选框后改变颜色，activebackground='背景色'，activeforeground='前景色'
        def word():
            print('以Word文档形式生成！')
        def pdf():
            print('以PDF文档形式生成！')
        def button():
            print('立即生成！')

        iv_click_colour = IntVar()
        self.rb_click_colour1 = Radiobutton(window, text='Word',value=1,command=word,variable=iv_click_colour,  # type: ignore
                                            activeforeground='blue')
        self.rb_click_colour2 = Radiobutton(window, text='PDF',value=2,command=pdf,variable=iv_click_colour,  # type: ignore 
                                            activeforeground='red')
        self.rb_click_colour1.place(x=450,y=170)
        self.rb_click_colour2.place(x=450,y=250)
        
        #立即生成按钮
        a=tk.Button(window,text="立即生成",width=15,command=button)  # type: ignore
        a.place(x=350,y=320)

        window.mainloop()
 
if __name__ == '__main__':
    radiobutton()
