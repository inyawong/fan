import linkSql as sql
import logging
from docxtpl import DocxTemplate
from Encryption import *


# db = pymysql.connect(
#     host="10.23.148.84",
#     port=3306,
#     user="root",
#     password="123456",
#     database="relocateFile",
#     charset="utf8")

#coding:utf-8
import sys
import os

#生成资源文件目录访问路径


def resource_path(relative_path):
    if getattr(sys, 'frozen', False):  # 是否Bundle Resource
        base_path = sys._MEIPASS
    else:
        base_path = os.path.abspath(".")
    return os.path.join(base_path, relative_path)


filepath = resource_path(os.path.join("PATH"))  # 读取资源文件夹PATH路径



# 查找函数-------------------------------------------------------------
def select_m(id):
    cursor = sql.db.cursor()

    sql_sel = "select * from mover where id='%s'"%id
    #sql_sel = "select ID, m_name, way, cooperative, m_add, tel from mover where id='%s'"%id
    result = cursor.execute(sql_sel)
    # print(cursor.fetchall())
    if result:
        mov = cursor.fetchall()
        cursor.close()
        return mov    
    else:
        print("未找到您的信息")
        return 1

    cursor.close()
    
    


# id = input("id: ")
# a = select_m(id)
# list = []
# b = None
# for b in a:
#     for c in b:
#         list.append(c)
# id=list[0]
# name = list[1]
# idNum = list[2]
# address = list[3]
# tel=list[4]
# area = list[5]
# total = list[6]
# doc = list[7]
# print(name)


def execute_sql(cursor, sql_str:str):
    try:
        return cursor.execute(sql_str)
    except Exception as e:
        logging.exception(e)
        return -1
    
# 修改函数-------------------------------------------------------------
def update_m(id,update_value):
    cursor = sql.db.cursor()
    s_result = cursor.execute("select * from mover where id='%s'"%id)

    if s_result:
        sql_update = "update mover set %s where id='%s';"%(update_value,id)
        # up_res = cursor.execute(sql_update) 
        up_res = execute_sql(cursor, sql_update)
        if up_res > 0:
            sql.db.commit()
            cursor.execute("select * from mover where id='%s'"%id)
            print("修改成功")
            print(cursor.fetchall())
        elif up_res <= 0:
            print("未成功修改")
    else:
        print("你想要修改的信息不存在")

    cursor.close()




def read(id):  #读取数据库
    cursor = sql.db.cursor()
    a = select_m(id)
    list = []
    for b in a:  # type: ignore
        for c in b:
            list.append(c)
    id=list[0]
    name = Decryption(list[1])
    idNum = Decryption(list[2])
    address = Decryption(list[3])
    tel = Decryption(list[4])
    area = Decryption(list[5])
    total = Decryption(list[6])
    # 测试print(name)
    # id = input("id: ") 测试
    # read(id)
    text = ''

    data_dic = {
    'owner': name,                      #乙方
    # 'cooperative': cooperative,
    # 'village': village,                     #明星村
    'add': address,
    'areaA': area,
    'areaB': '/',
    'areaC': '/',
    'homesteadArea': '/',
    'id': id,
    'total': total,
    # 'total1': tal1,
    # 'firstPay': firPay,
    # 'firPay1': fir1,
    # 'secondPay': secPay,
    # 'secPay1': sec1,
    # 'bank': bank,
    'accName': name,
    # 'accNumber': accNumber,
    # 'address': address,
    'idNum': idNum,
    'tel': tel,
    # 'way': way,
    'text': text
    }
    doc = DocxTemplate('template/1.docx')  # 加载模板文件
    doc.render(data_dic)  # 填充数据
    path = 'template/' + id + ' ' + name + '协议.docx'  # type: ignore
    doc.save(path)  # 保存目标文件
    # reDo.test_insert_a_doc(id, name, path)
    print("Document Done!")
    print(text)
    print('read:', id, name)

# id = input("请输入id：")
# read(id)


# -------------------------------------------------------------
# id = input("id: ") 
# up_key = input("请输入你要修改的内容: ")
# update_m(id,up_key)
# -------------------------------------------------------------




# 增加函数-------------------------------------------------------------
def insert_m(id,name,idnum,address,tel,area,total):
    cursor = sql.db.cursor()
    sql_insert = "insert into mover (id,m_name,idnum,address,tel,area,total) values('%s','%s','%s','%s','%s','%s','%s')" % (
        id,
        Encryption(name),
        Encryption(idnum),
        Encryption(address),
        Encryption(tel),
        Encryption(area),
        Encryption(total)
        )
    cursor.execute(sql_insert)
    sql.db.commit()   # 一定要commit
    cursor.close()
    print('插入数据成功')
    return 1
    sql.db.close()
#测试
# id =input("请添加用户id: ")
# name =input("请添加用户名：")
# insert_m(id,name)


#删除函数-------------------------------------------------------------
def delete_m(id):
    cursor = sql.db.cursor()
    print(id)
    sql_delete = "delete from mover where id='%s'"%id
    result= cursor.execute(sql_delete) 
    sql.db.commit() 
    cursor.close()
    if result:
        print('删除数据成功')
        return 1
    else:
        print("id不存在，未找到该id的用户信息")
        return 0
#测试
#id =input("请输入要删除的用户id: ")
#delete_m(id)


def load_file():
    cursor = sql.db.cursor()
    sql_load = 'SELECT * FROM mover;'
    cursor.execute(sql_load)
    res = cursor.fetchall()
    print(res)
    list2 = []
    for i in res:  # type: ignore
        print(i)
        a = []
        for c in i:
            print(c)
            a.append(c)
        list2.append(a)  # type: ignore
    print(list2)
load_file()