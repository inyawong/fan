from tkinter import *  # type: ignore
from tkinter.ttk import *  # type: ignore
from tkinter.messagebox import *  # type: ignore
import os
import basicOperate as bo

class DetailWindow(Toplevel):
    def __init__(self,action_flag:int,current_mover:list,all_mover_list:list):
        super().__init__()
        self.title("数据增添")
        self.geometry("600x500+600+150")
        self.resizable(0,0)# type: ignore # 不能改变大小

        # 定义全局变量
        self.flag = action_flag
        self.current_mover_list = current_mover
        self.all_mover_list = all_mover_list
        # 加载控件
        self.setup_UI()
        # 修改明细窗口标题
        self.load_windows_flag()

    def setup_UI(self):
        # 设置style
        self.Style01 = Style()
        self.Style01.configure("title.TLabel",font=("华文黑体",20,"bold"),foreground = "navy")
        self.Style01.configure("detail.TLabel", font=("华文黑体", 12, "bold"), foreground="navy")
        self.Style01.configure("TButton",font=("华文黑体",12,"bold"),foreground = "navy")
        self.Style01.configure("TEntry", font=("华文黑体", 12, "bold"),width = 10)
        self.Style01.configure("TRadiobutton",font=("华文黑体",12,"bold"),foreground = "navy")
        # 加载上面的banner
        self.Login_image = PhotoImage(file = "."+os.sep+"img"+os.sep+"stu_detail_banner.png")
        self.Label_image = Label(self,image = self.Login_image)
        self.Label_image.pack()

        # 添加一个title
        self.var_title = StringVar()
        self.Label_title = Label(self,text="==数据增添==",style = "title.TLabel")# type: ignore
        self.Label_title.place(x=360,y=20)

        # 加载一个pane
        self.Pane_detail = PanedWindow(self,width = 590,height = 380)
        self.Pane_detail.place(x = 5,y = 88)

        # 加一个框
        self.Label_picture= Text(self,height=23,width=20)
        self.Label_picture.place(x=400,y=100)

    
        # 添加属性
        # 第一排：ID
        self.Label_id = Label(self.Pane_detail,text = "ID:",style = "detail.TLabel")# type: ignore
        self.Label_id.place(x=30,y=10)
        self.var_id = StringVar()
        self.Entry_id = Entry(self.Pane_detail,textvariable = self.var_id,font=("华文黑体", 15, "bold"),width = 20)
        self.Entry_id.place(x=130,y=8)
        # 姓名
        self.Label_name = Label(self.Pane_detail, text="姓名:", style="detail.TLabel")# type: ignore
        self.Label_name.place(x=30, y=60)
        self.var_name = StringVar()
        self.Entry_name = Entry(self.Pane_detail, textvariable=self.var_name, font=("华文黑体", 15, "bold"), width=20)
        self.Entry_name.place(x=130, y=58)
        # 身份证号
        self.Label_idnum = Label(self.Pane_detail, text="身份证号:", style="detail.TLabel")# type: ignore
        self.Label_idnum.place(x=30, y=110)
        self.var_idnum = StringVar()
        self.Entry_idnum = Entry(self.Pane_detail, textvariable=self.var_idnum, font=("华文黑体", 15, "bold"), width=20)
        self.Entry_idnum.place(x=130, y=108)
        # 电话号码
        self.Label_mobile = Label(self.Pane_detail, text="电话号码:", style="detail.TLabel")# type: ignore
        self.Label_mobile.place(x=30, y=160)
        self.var_mobile = StringVar()
        self.Entry_mobile = Entry(self.Pane_detail, textvariable=self.var_mobile, font=("华文黑体", 15, "bold"), width=20)
        self.Entry_mobile.place(x=130, y=158)
        # 面积
        self.Label_area = Label(self.Pane_detail, text="面积:", style="detail.TLabel")# type: ignore
        self.Label_area.place(x=30, y=210)
        self.var_area = StringVar()
        self.Entry_area = Entry(self.Pane_detail, textvariable=self.var_area, font=("华文黑体", 15, "bold"), width=20)
        self.Entry_area.place(x=130, y=208)
        # 金额
        self.Label_money = Label(self.Pane_detail, text="金额:", style="detail.TLabel")# type: ignore
        self.Label_money.place(x=30, y=260)
        self.var_money = StringVar()
        self.Entry_money = Entry(self.Pane_detail, textvariable=self.var_money, font=("华文黑体", 15, "bold"), width=20)
        self.Entry_money.place(x=130, y=258)
        # 地址
        self.Label_address = Label(self.Pane_detail, text="地址:", style="detail.TLabel")# type: ignore
        self.Label_address.place(x=30, y=310)
        self.var_address = StringVar()
        self.Entry_address = Entry(self.Pane_detail, textvariable=self.var_address, font=("华文黑体", 15, "bold"), width=20)
        self.Entry_address.place(x=130, y=308)

        # 放置两个按钮
        self.Button_save = Button(self,text = "保存",style = "TButton",command = self.commit)# type: ignore
        self.Button_save.place(x=300,y=472)
        self.Button_exit = Button(self,text = "关闭",style = "TButton",command = self.close_window)# type: ignore
        self.Button_exit.place(x=450,y=472)

    def load_windows_flag(self):
        if self.flag == 1:
            # 修改title
            self.Label_title.configure(text="==详细数据==")
            # 加载数据
            self.load_mover_detail()
            # 控制控件的状态
            self.Button_save.place_forget()
            self.Entry_id["state"] = DISABLED
            self.Entry_name["state"] = DISABLED
            self.Entry_idnum["state"] = DISABLED
            self.Entry_mobile["state"] = DISABLED
            self.Entry_area["state"] = DISABLED
            self.Entry_money["state"] = DISABLED

        elif self.flag == 2:
            self.Label_title.configure(text="==数据增添==")
        elif self.flag == 3:
            self.Label_title.configure(text="==修改数据==")
            # 填充数据
            self.load_mover_detail()
            # 学号不允许修改
            self.Entry_id["state"] = DISABLED

    def load_mover_detail(self):
        print(self.current_mover_list)
        if len(self.current_mover_list) == 0:
            showinfo("系统消息","没有任何数据需要展示!")
        else:
            self.var_id.set(self.current_mover_list[0])  # 编号
            self.var_name.set(self.current_mover_list[1])  # 姓名
            self.var_idnum.set(self.current_mover_list[2])  # 身份证号
            self.var_address.set(self.current_mover_list[3])  # 地址
            self.var_mobile.set(self.current_mover_list[4])  # 电话号码
            self.var_area.set(self.current_mover_list[5])  # 面积A
            self.var_money.set(self.current_mover_list[6])  # 金额
            self.var_mobile.set(self.current_mover_list[9])

    def close_window(self):
        """
        关闭当前窗  体
        :return:
        """
        self.userinfo = 0
        self.destroy()

    def commit(self):
        if self.flag == 1:  # 查看
            pass
        elif self.flag == 2:    # 添加
            # 准备数据
            temp_list = []
            if len(str(self.Entry_id.get()).strip()) == 0:
                showinfo("系统消息","编号不能为空！")
            else:
                temp_list.append(str(self.Entry_id.get()).strip())
                temp_list.append(str(self.Entry_name.get()).strip())
                temp_list.append(str(self.Entry_mobile.get()).strip())
                temp_list.append(str(self.Entry_idnum.get()).strip())
                temp_list.append(str(self.Entry_area.get()).strip())
                temp_list.append(str(self.Entry_money.get()).strip())
                temp_list.append(str(self.Entry_address.get()).strip())

                # print(temp_list)

                # 添加到all_mover_list
                self.all_mover_list.append(temp_list)
                bo.insert_m(temp_list[0], temp_list[1],temp_list[3],temp_list[6],temp_list[2],temp_list[4],temp_list[5])
                # 提醒添加成功
                showinfo("系统消息","客户信息添加成功")
                # 反馈信号给主窗体
                self.userinfo = 1
                # 关闭窗体
                self.destroy()
        elif self.flag == 3:    # 修改
            # 把当前界面中的数据存储在集合中
            temp_list = []
            if len(str(self.Entry_id.get()).strip()) == 0:
                showinfo("系统消息", "ID不能为空！")
                return
            else:
                temp_list.append(str(self.Entry_id.get()).strip())
                temp_list.append(str(self.Entry_name.get()).strip())
                temp_list.append(str(self.Entry_mobile.get()).strip())
                temp_list.append(str(self.Entry_id.get()).strip())
                temp_list.append(str(self.Entry_area.get()).strip())
                temp_list.append(str(self.Entry_money.get()).strip())
                temp_list.append(str(self.Entry_address.get()).strip())
                # 遍历集合
                for index in range(len(self.all_mover_list)):
                    if self.all_mover_list[index][0] == self.current_mover_list[0]:
                        self.all_mover_list[index] = temp_list
                
                bo.update_m(temp_list[0], temp_list)
                # 反馈信息
                # 提醒
                showinfo("系统消息","客户信息修改成功！")
                # 反馈信号给主窗体
                self.userinfo = 1
                # 关闭窗体
                self.destroy()

if __name__ == '__main__':
    this_window = DetailWindow()  # type: ignore
    this_window.mainloop()
