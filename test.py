# -*- coding: utf-8 -*-
import apscheduler
import time
from apscheduler.schedulers.blocking import BlockingScheduler
import pandas as pd
import pymysql
import subprocess
import os


def my_job():
    # 用Popen进程操作打开.py文件
    subprocess.Popen(
        'python F:/backtest/basic-check/basic-check/test_kang.testmysql.py')
    a = pd.read_sql(sqlcmd, dbconn)  # 用pandas中的read_sql读取mysql数据
    b = a.head()
    print(b)

   # print (time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(time.time())))
  #  subprocess.popen()


sched = BlockingScheduler()
sched.add_job(my_job, 'interval', seconds=5)
sched.start()
