# !/usr/bin/python
#-*-coding:utf-8-*-
import pytesseract  # ocr字符识别库
import cv2
from PIL import Image
import sys

import time
time1 = time.time()
###########二值化算法


def binarizing(img, threshold):
    pixdata = img.load()
    w, h = img.size
    for y in range(h):
        for x in range(w):
            if pixdata[x, y] < threshold:
                pixdata[x, y] = 0
            else:
                pixdata[x, y] = 255
    return img

###########去除干扰线算法


def depoint(img):  # input: gray image
    pixdata = img.load()
    w, h = img.size
    for y in range(1, h-1):
        for x in range(1, w-1):
            count = 0
            if pixdata[x, y-1] > 245:
                count = count + 1
            if pixdata[x, y+1] > 245:
                count = count + 1
            if pixdata[x-1, y] > 245:
                count = count + 1
            if pixdata[x+1, y] > 245:
                count = count + 1
            if count > 2:
                pixdata[x, y] = 255
    return img
########身份证号码识别


def identity_OCR(pic_path):
    #####身份证号码截图
    img1 = Image.open(pic_path)
    # w, h = img1.size
    # ##将身份证放大3倍
    # out = img1.resize((w*3, h*3), Image.ANTIALIAS)
    # Image._show(out)  # type: ignore
    # cv2.waitKey(0)
    region = (125*3, 200*3, 370*3, 250*3)
    # #裁切身份证号码图片
    # cropImg = out.crop(region)
    # print(cropImg)
    # Image._show(cropImg)
    # cv2.waitKey(0)
    # 转化为灰度图
    img = img1.convert('L')
    Image._show(img)  # type: ignore
    cv2.waitKey(0)
    # 把图片变成二值图像。
    img1 = binarizing(img, 100)
    img2 = depoint(img)
    # 去除干扰
    code = pytesseract.image_to_string(img2,lang='chi_sim')
    print("识别该身份证信息是:"+str(code))

'''
#####################中文图片识别###############################
要在pytesseract 库的 image_to_string() 方法里加个参数lang='chi_sim'，
这个就是引用对应的中文语言包，中文语言包的全名是 chi_sim.traineddata
'''


def identity_OCR_Chine(pic_path):
    from PIL import Image
    import pytesseract
    image = Image.open(pic_path)
    content = pytesseract.image_to_string(image, lang='chi_sim')  # 解析图片
    print(str(content))



if __name__ == '__main__':
    pic_path = "./testSample/idcard6.jpg"
    # identity_OCR(pic_path)
    identity_OCR_Chine(pic_path)
    # identity_OCR_Nopro(pic_path)

    time2 = time.time()
    print(u'总共耗时：' + str(time2 - time1) + 's')
