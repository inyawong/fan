import string
import hashlib

#记录密码
passwd=''
def GetPasswd(inputtedPasswd: str):
    global passwd
    passwd=inputtedPasswd

def GetPasswdHash(passwd: str):
    result = hashlib.md5(passwd.encode("utf-8")).hexdigest()
    return result


def PasswdVerify(passwd: str, md5: str):
    if hashlib.md5(passwd.encode("utf-8")).hexdigest() == md5:
        return True
    else:
        return False


def Encryption(data: str):
    dataLength = len(data)
    if dataLength < 1:
        return
    result = ''
    for i in range(0, 10):
        for passwdIndex in range(0, len(passwd)):
            index = i*len(passwd)+passwdIndex
            result += str(ord(passwd[passwdIndex]) ^ ord(data[index]))
            result += ' '
            if index >= dataLength-1:
                return result


def Decryption(data: str):
    binData = data.split(' ')
    print(binData)
    result = ''
    for i in range(0, 10):
        for passwdIndex in range(0, len(passwd)):
            index = i*len(passwd)+passwdIndex
            result += chr(int(binData[index]) ^ ord(passwd[passwdIndex]))
            if index >= len(binData)-2:
                return result


# -------------Test only--------------

#print(Encryption("Password", "This is data"))
#print(Decryption("Password", "4 9 26 0 87 6 1 68 52 0 7 18"))
#print(GetPasswdHash("123456"))
#print(PasswdVerify("Password","dc647eb65e6711e155375218212b3964"))
