from docxtpl import DocxTemplate
import os
import openpyxl

##########填写##########
name = 'G'                            #乙方
# way = '安置'
id = 'A'
# cooperative = '建兴'                     #合作社
# village = '明星'                     #明星村
add = '青云街一巷14号'
areaA = '467.0473'
areaB = '  /  '
areaC = '  /  '
# homesteadArea = '103.6000'
# bank = '广州农村商业银行股份有限公司'
# accNumber = '0204 0401 0180 003784'
idNum = '440111195311281559'
tel = '13710898918'
# address = '广州市白云区明星松园街一巷6号'

total = 1352814.37
award = 166384.48
settle = 67200.00

# if homesteadArea == '  /  ':
text = ''
# else:
#     text = '、宅基地证复印件'


###############计算############
secondPay = award + settle
firstPay = -(secondPay - total)

def change(a):
    a1 = format(float(a), '.2f')
    a2 = format(float(a1), ',')
    a_len = len(a2.split('.')[1])
    if a_len == 1:
        a3 = a2 + '0' # 补0
    else:
        a3 = a2
    return a3


tal = change(total)
firPay = change(firstPay)
secPay = change(secondPay)

##########################大小写########
def _rmb_upper(value):
    """
    人民币大写
    来自：http://topic.csdn.net/u/20091129/20/b778a93d-9f8f-4829-9297-d05b08a23f80.html
    传入浮点类型的值返回 unicode 字符串
    """
    map = [u"零", u"壹", u"贰", u"叁", u"肆", u"伍", u"陆", u"柒", u"捌", u"玖"]
    unit = [u"分", u"角", u"元", u"拾", u"佰", u"仟", u"万", u"拾", u"佰", u"仟", u"亿",
            u"拾", u"佰", u"仟", u"万", u"拾", u"佰", u"仟", u"兆"]

    nums = []  # 取出每一位数字，整数用字符方式转换避大数出现误差
    for i in range(len(unit) - 3, -3, -1):
        if value >= 10 ** i or i < 1:
            nums.append(int(round(value / (10 ** i), 2)) % 10)

    words = []
    zflag = 0  # 标记连续0次数，以删除万字，或适时插入零字
    start = len(nums) - 3
    for i in range(start, -3, -1):  # 使i对应实际位数，负数为角分
        if 0 != nums[start - i] or len(words) == 0:
            if zflag:
                words.append(map[0])
                zflag = 0
            words.append(map[nums[start - i]])
            words.append(unit[i + 2])
        elif 0 == i or (0 == i % 4 and zflag < 3):  # 控制‘万/元’
            words.append(unit[i + 2])
            zflag = 0
        else:
            zflag += 1

    if words[-1] != unit[0]:  # 结尾非‘分’补整字
        words.append(u"整")
    return ''.join(words)

tal1 = _rmb_upper(total)
fir1 = _rmb_upper(firstPay)
sec1 = _rmb_upper(secondPay)




###############填充数据#########
data_dic = {
'owner': name,                      #乙方
# 'cooperative': cooperative,
# 'village': village,                     #明星村
'add': add,
'areaA': areaA,
'areaB': areaB,
'areaC': areaC,
# 'homesteadArea': homesteadArea,
'id': id,
'total': tal,
'total1': tal1,
'firstPay': firPay,
'firPay1': fir1,
'secondPay': secPay,
'secPay1': sec1,
# 'bank': bank,
'accName': name,
# 'accNumber': accNumber,
# 'address': address,
'idNum': idNum,
'tel': tel,
# 'way': way,
'text': text
}

doc = DocxTemplate('template/temtemplateAgree.docx') #加载模板文件
doc.render(data_dic) #填充数据
doc.save(id + ' ' + name + '协议.docx') #保存目标文件
print("Document Done!")

print(text)

##############################################################

# workbook = openpyxl.load_workbook('template/补偿签收表.xlsx')  # 返回一个workbook数据类型的值
# sheet = workbook.active  # 获取活动表

# # sheet['C3'] =  '经济社'
# sheet['D3'] = name
# sheet['E3'] = id
# sheet['F3'] = idNum
# sheet['G3'] = tel
# # sheet['H3'] = bank
# # sheet['I3'] = accNumber
# sheet['J3'] = total  # type: ignore
# workbook.save(id + ' ' + name + '补偿签收表.xlsx')

# print("签署表 Done!")
print(id + ' ' + name)
