import pytesseract  # ocr字符识别库
import cv2
from PIL import Image
import sys
import time
time1 = time.time()


def identity_OCR_Chine(pic_path):
    from PIL import Image
    import pytesseract
    image = Image.open(pic_path)
    content = pytesseract.image_to_string(image, lang='chi_sim')  # 解析图片
    print(str(content))


if __name__ == '__main__':
    pic_path = "./testSample/idcard6.jpg"
    # identity_OCR(pic_path)
    identity_OCR_Chine(pic_path)
    # identity_OCR_Nopro(pic_path)

    time2 = time.time()
    print(u'总共耗时：' + str(time2 - time1) + 's')
