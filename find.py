import tkinter as tk
from tkinter import ttk
import basicOperate as bo
import tkinter.messagebox  as msg# 弹窗库
window = tk.Tk()
window.geometry("1080x530")
window.title("FAN·信息处理系统")
window.configure(bg='darkgray')
# img = Image.open('1.png')
#icon = ImageTk.PhotoImage(img)
# window.iconbitmap('icon') #设置左上角小图标


a = tk.Label(window, width=500, height=300,bg='white')
a.place(x=0, y=0)

b = tk.Label(window,width=0,height=300,bg='gray')
b.place(x=180,y=0)

c = tk.Label(window,width=320,height=0,bg='gray')
c.place(x=180, y=160)

title1 = tk.Label(window, text='数据处理/查询', font=('黑体',8), bg='white')
title1.place(x=190, y=20)

title = tk.Label(window, text='查询', font=('黑体', 10), bg='white')
title.place(x=190, y=40)

v_id = tk.StringVar()
label1 = tk.Label(window, text='  id ： ', font=('宋体', 12))
label1.place(x=400, y=100)
v_id = tk.Entry(window, textvariable=v_id,  width=30,
                bd=4, highlightcolor='black', bg='#f0f8f9')
v_id.place(x=480, y=100)



'''
表格
'''
def onSelect(e):
    ss = tree.selection()
    global index
    index = []
    for s in ss:
        itm = tree.set(s)
        print(itm)
        index.append(itm['id'])
        print('id:',index)
    pass

tree = ttk.Treeview(window, columns=(
    "id",  "姓名", "安置", "合作社", "地址", "电话号码"), show="headings", displaycolumns="#all")      # #创建表格对象
tree.column("id", width=80)
tree.column("姓名", width=80)                         
tree.column("安置", width=80)                         
tree.column("合作社", width=200)
tree.column("电话号码", width=150)

tree.heading("id", text="id")
tree.heading("姓名", text="姓名")        # #设置显示的表头名
tree.heading("安置", text="安置")
tree.heading("合作社", text="合作社")
tree.heading("地址", text="地址")
tree.heading("电话号码", text="电话号码")

tree.bind("<<TreeviewSelect>>", onSelect)
print()

# a = bo.select_m(id='MXZ3S1109')
# print(a)
# a = list(a)
# print(a)
# tree.insert("", 0, text="line1", values=a[0])

tree.place(x=240, y=200)

# 删除-----------------------------------------------------------
def delete():
    # id = v_id.get()
    # print(id)
    global index
    result = []
    idList = []
    falList = []
    for i in range(len(index)):
        sql_id = str(index[i])
        res = bo.delete_m(sql_id)
        result.append(res)
    for j in range(len(result)):
        if result[j]:
            idList.append(result[j])
        else:
            falList.append(result[j])
    if idList:
        msg.showinfo('提示',  "删除成功!")
    else:
        msg.showerror('错误', '删除失败。')
bu2 = tk.Button(window, text='删除', command=delete)  # type: ignore
bu2.place(x=350, y=450)


def update():
    id = v_id.get()
    print(id)
    res = bo.update_m(id,update_value='0')

    if res:
        msg.showinfo('提示', '修改成功!')
    else:
        msg.showerror('错误','修改失败。')
bu3 = tk.Button(window, text='修改', command= update)  # type: ignore
bu3.place(x=590, y=450)

def reset():
    def delButton(tree):
        x = tree.get_children()
        for item in x:
            tree.delete(item)
    delButton(tree)
bu4 = tk.Button(window, text='重置', command=reset)  # type: ignore
bu4.place(x=830, y=450)

def find():
    id = v_id.get()
    print(id)
    res = bo.select_m(id)
    if res:
        print(res)
        res = list(res)
        print(res)
        tree.insert("", 0, text=id, values=res[0])
    else:
        msg.showerror('错误','查询失败。')
bu1 = tk.Button(window, text='  查询  ', command= find)  # type: ignore
bu1.place(x=750, y=98)

# 1、读取word文档 -> 怎么读入doc <- Docxtemplate
# 2、写入数据库 -> 怎么写入数据库 <- mysql 接口。sql.execute

window.mainloop()